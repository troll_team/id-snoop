trollMark = "TROLL_MARK";

function genHTTPCookie(value) {
    var d = new Date();
    d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    var expir = "expires=" + d.toUTCString();
    return trollMark + "=" + value + ';' + expir;
}

// HTTP Cookie
function setHTTPCookie(value) {
    console.log('HTTP');
    document.cookie = genHTTPCookie(value);
}

function getHTTPCookie() {
    if (document.cookie.length) {
        var name = trollMark + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; ++i) {
            var c = ca[i];
            if (c.charAt(0) == ' ') {
                c = c.substr(1);
            }
            if (c.indexOf(name) != -1) {
                return c.substr(name.length, c.length);
            }
        }
    }
    return null;
}
//

// Local Storage
function setLocalStorageMark(value) {
    window.localStorage.setItem(trollMark, value);
}

function getLocalStorageMark(value) {
    return window.localStorage.getItem(trollMark);
}
//