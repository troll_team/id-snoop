# README #

## Результат ##
Приложение опубликовано здесь: http://id-snoop.appspot.com/

## Цель ##
Реализовать сайт, умеющий определять уникальных посетителей.

### Три страницы ###
1. Главная страница, которая делает фингерпринтинг браузера и ставит уникальную метку; максимальное количество возможной информации о посетившем и проставленная метка сохраняется в БД для дальнейшего анализа.
2. Страница, на которой можно посмотреть все проставленные метки в данный браузер. Страница должна иметь вид таблицы из двух столбцов: тип проставляемой метки (cookie, localStorage и т.п.) и ее значение.
3. Страница, на которой в виде таблицы отображены все обращения к главной странице, и на которой можно выбрать два различных посещения главной страницы и запросить у сервера сравнение их браузеров и операционных систем. 

### Характеристики ###
* Первая группа: 
    * IP-адрес, заголовок User-Agent
    * http-заголовки x_real_ip, x_forwarded_for
    * определение браузерного движка с помощью JavaScript
    * поддержка html5
    * разрешение экрана с помощью JavaScript
    * flash capabilities

* Вторая группа:
    * плагины браузера
    * flash микрофоны и камеры
    * flash шрифты

### Условия ###

* *Корректность для первой группы характеристик.* 
    Пары таких характеристик считаются корректно работающими, если система обнаруживает несоответствие внутри пары.
    Каждая характеристика может находится в любом количестве пар. 
    Характеристика считается корректно работающей, если она присутствует хотя бы в одной корректной паре. 

* *Корректность для второй группы характеристик.*
    Характеристики считаются корректно работающими, если система может выделить из них уникальную информацию для каждой метки на основе проставленных меток.

* *Пользователи не подделывают метки.*
* *Пользователи могут очищать метки.*
* *Пользователи можут изменять любые характеристики, доступные для изменения.*


### Формат третьей страницы ###
1. Таблица обращений к главной странице:
    * время
    * ip-адрес
    * заголовок User-Agent
    * проставленная метка
2. Система сравнения двух различных посещений главной страницы. Для первой группы характеристик необходимо проверять совпадение значений.

## Задачи ##

### Минимальные требования ###
* возможность отличить Chrome от FireFox без использования заголовка User-Agent и меток
* добавление метки в браузер с помощью cookies, localStorage, FlashCookies
* блокировка доступа к сайту с помощью TOR (return HTTP 402)

### +50 баллов ###
Постановка уникальной метки с помощью каждого из следующих  механизмов:

* png generation w/forced cache and html5 canvas pixel reading
* http etags
* http cache
* window.name
* html5 session cookies
* html5 global storage
* html5 database storage
* css history scanning


### + 30 баллов ###
Использование следующих характеристик для определения уникальности браузера:

* IP-адрес, заголовок User-Agent
* список плагинов браузера
* разрешение экрана с помощью JavaScript
* http-заголовки x_real_ip, x_forwarded_for
* flash capabilities
* flash микрофоны и камеры
* flash шрифты
* определение браузерного движка с помощью JavaScript


### +60 баллов ###
Определение поддержки HTML5 (https://html5test.com/compare/browser/chrome-39/firefox-34/ie-11/opera-24/safari-8.0.html)


### +400 баллов ###
*Clickjacking.* Засчитывается только тот, который позволяет узнать имя пользователя на некотором популярном сайте (из alexa top 500 ru или global)

### +100 баллов ###
Определения факта залогиненности пользователя на некотором популярном сайте (из alexa top 500 ru или global) без разрешения на это пользователя

### +100-300 баллов ###
Вероятностное определение "похожести" браузеров

### +50 баллов ###
Дополнительные механизмы простановки меток (механизмы, используемые в evercookie не будут засчитаны, за исключением тех, которые перечислены выше)

### +50 баллов ###
Дополнительные способы определения несоответствия характеристик браузера

### +50 баллов ###
Дополнительные механизмы получения уникальных данных

### Полезные ссылки ###
* Troll-team doc: http://goo.gl/t4jVql 
* Презентация с семинара: http://bit.ly/1BvOXQH
* Список узлов TOR: https://check.torproject.org/exit-addresses
* Panopticlick: https://panopticlick.eff.org/
* p0f: http://lcamtuf.coredump.cx/p0f3/
* evercookie: http://samy.pl/evercookie/
* взаимодействие между flash и страницей: http://javascript.ru/unsorted/bridge-to-flash