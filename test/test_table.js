function TestList(tid) {
    var ll = document.createElement('ul');
    ll.id = tid;
    document.body.appendChild(ll);

    this.addLine = function (name, value) {
        var msg = document.createElement('li');
        msg.innerHTML = '[' + name + '] -> ' + value;
        document.getElementById(tid).appendChild(msg);
    };
}