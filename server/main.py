import webapp2
import jinja2
import time
import os
import cgi
import random, struct, hashlib
from google.appengine.ext import db

# CSRF Gen
def gen_csrf():
    hs = hashlib.sha1()
    hs.update(struct.pack('<I', random.randint(0, 1 << 32 - 1)))
    return hs.hexdigest()

# AntiTOR
from urllib2 import urlopen
import re
class AntiTOR:
    def __init__(self):
        page = urlopen("https://check.torproject.org/exit-addresses")
        data = page.read()

        ip_adresses = re.findall(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", data)
        self.ips = set(ip_adresses)

    def notTor(self, ip):
        print ip
        return ip not in self.ips

    def protect(self, fn):
        def protected(*args, **kwargs):
            if self.notTor(os.environ['REMOTE_ADDR']):
                fn(*args, **kwargs)
            else:
                r = args[0]
                r.status_int = 402
        protected.func_name = fn.func_name
        return protected

antiTor = AntiTOR()


# Jinja
templs = os.path.join(os.path.dirname(__file__), 'template')
jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(templs), autoescape=True)

# Data
class Client(db.Model):
    time_stamp = db.DateTimeProperty()
    id = db.StringProperty(indexed=True)
    time = db.StringProperty()
    ip = db.StringProperty()
    user_agent = db.StringProperty()
    mark = db.StringProperty()
    data = db.BlobProperty()

# Pages' data
with open('fp.html') as f:
    main_page = f.read()
with open('marks.html') as f:
    marks_page = f.read()

# Handlers
class FrontPage(webapp2.RequestHandler):
    @antiTor.protect
    def get(self):
        # mark & csrf_token
        nmark = 'xzx%dxzx' % int(time.time())
        # resp
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(main_page.format(nmark))

class MarksPage(webapp2.RequestHandler):
    @antiTor.protect
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(marks_page)

class LogPage(webapp2.RequestHandler):
    @antiTor.protect
    def get(self):
        clns = Client.all()
        clns.order('time')
        args = { 'clients' : clns }

        lp = jenv.get_template('log.html')
        lp_txt = lp.render(**args)

        self.response.headers['Content-Type'] = 'text/html'
        self.response.out.write(lp_txt)

class SendAPI(webapp2.RequestHandler):
    @antiTor.protect
    def post(self):
        req = self.request
        if req.content_type == 'application/json':
            mark = req.headers['TrollMark']
            cl = Client(\
                id = gen_csrf(),
                time = time.asctime(),
                ip = os.environ['REMOTE_ADDR'], \
                user_agent = cgi.escape(req.headers['User-Agent']), \
                mark = cgi.escape(mark), \
                data = cgi.escape(req.body) \
            )
            cl.put()

class RecvAPI(webapp2.RequestHandler):
    @antiTor.protect
    def get(self):
        req = self.request
        id = req.GET['id']
        # if re.match(r'xzx\d+xzx', mark):
        clns = Client.all()
        clns.filter('id =', id)
        cl = clns.get()
        self.response.headers['Content-Type'] = 'text/html'
        self.response.out.write(cl.data)

# Run
application = webapp2.WSGIApplication([
    ('/', FrontPage),
    ('/marks', MarksPage),
    ('/log', LogPage),
    ('/send', SendAPI),
    ('/recv', RecvAPI),
], debug=False)
