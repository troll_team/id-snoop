function List(parent) {
    var ll = document.createElement('ul');
    parent.appendChild(ll);

    this.addLine = function (name, value) {
        var msg = document.createElement('li');
        msg.innerHTML = name + ' -> ' + value;
        ll.appendChild(msg);
    };
}