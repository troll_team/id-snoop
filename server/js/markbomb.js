(function() {
    var troll_mark = "TROLL_MARK";
    var mark_val = document.getElementById('mark').value;
    var ec = new evercookie({});
    ec.get(troll_mark, function (best, all) {
        if (/^xzx[0-9]+xzx$/.test(best)) {
            mark_val = best;
        }
        ec.set(troll_mark, mark_val);
        launchSnoop(mark_val);
    });
})();
