function showComparison() {

    var mt = document.getElementById('log');
    var inps = mt.getElementsByTagName('input');
    var ids = [];
    for (var i = 0; i < inps.length; ++i) {
        if (inps[i].checked) {
            ids.push(inps[i].id);
        }
    }
    if (ids.length == 2) {
        generateComparisonTable(ids[0], ids[1]);
        // window.location = '#cwin';
    } else {
        alert("Choose TWO entries !!!");
    }

    function getDataById(id) {
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET', '/recv?id=' + id, false);
        xmlhttp.send(null);
        if (xmlhttp.status == 200) {
            return JSON.parse(xmlhttp.responseText);
        } else {
            return undefined;
        }
    }

    function generateComparisonTable(id1, id2) {
        // Clear
        var comp_el = document.getElementById('comp');
        while (comp_el.firstChild) {
            comp_el.removeChild(comp_el.firstChild);
        }

        // Get Data
        var first = getDataById(id1), second = getDataById(id2);
        var plus = 0, minus = 0;

        // Generate
        comp_el.innerHTML = master_render() + js_render() +
            specials_render() + fonts_render() + score_render();
        comp_el.style.visibility = 'visible';

        function header() {
            return '' +
                '<table border="1">' +
                '<tr></tr>' +
                '<th> Property </th>' +
                '<th>' + id1 + '</th>' +
                '<th>' + id2 + '</th>';
        }

        function master_render() {
            var fm = first['master'], sm = second['master'];
            return '' +
                '<div>' +
                '<p>Master</p>' +
                header() +
                    '<tr></tr>' +
                    '<td>' +
                    ((fm == sm) ? '+ ' : '- ') +
                    'Browser' +
                    '</td>' +
                    '<td>' + fm + '</td>' +
                    '<td>' + sm + '</td>' +
                '</table>' +
                '</div>';
            return res;
        }

        function build_comp_table(name, fd, sd, ftbl) {
            var res = '' +
                '<div>' +
                '<p>' + name + '</p>' +
                header();

            for (var idx = 0; idx < fd.length; ++idx) {
                var nm = ftbl[idx];
                var val1 = fd[idx], val2 = sd[idx];
                var eq = val1 == val2;
                eq ? plus++ : minus++;
                res += '' +
                    '<tr></tr>' +
                    '<td>' +
                    (eq ? '+ ' : '- ') +
                    nm +
                    '</td>' +
                    '<td>' + val1 + '</td>' +
                    '<td>' + val2 + '</td>';
            }
            res += '</table>';
            return res;
        }

        function js_render() {
            return build_comp_table(
                'Java Script',
                first['js'],
                second['js'],
                feat_names['js']
            );
        }

        function specials_render() {
            var fs = first['specials'], ss = second['specials'];
            return '' +
                '<div>' +
                '<p>Specials</p>' +
                build_comp_table(
                    'CSS',
                    fs['css'],
                    ss['css'],
                    feat_names['css']
                ) +
                build_comp_table(
                    'HTML5',
                    fs['html'],
                    ss['html'],
                    feat_names['html']
                ) +
                build_comp_table(
                    'Misc',
                    fs['misc'],
                    ss['misc'],
                    feat_names['misc']
                ) +
                build_comp_table(
                    'Non Core',
                    fs['noncore'],
                    ss['noncore'],
                    feat_names['noncore']
                ) +
                '</div>';
        }

        function fonts_render() {
            function collectFonts(d) {
                var df = d['fonts'];
                var dr = [];
                for (var i = 0; i < df.length; ++i) {
                    dr.push(fontBase[df[i]])
                }
                return dr.join('\n');
            }

            var ff = collectFonts(first), sf = collectFonts(second);
            var eq = ff == sf;
            eq ? plus++ : minus++;
            return '' +
                '<div>' +
                    '<p>Fonts</p>' +
                    header() +
                        '<tr></tr>' +
                        '<td>' +
                        ((ff == sf) ? '+' : '-') +
                        '</td>' +
                        '<td>' + ff + '</td>' +
                        '<td>' + sf + '</td>' +
                    '</table>' +
                '</div>';
        }

        function score_render() {
            return '' +
                '<div>' +
                    '<p>Total score: ' + (1.0 * plus / (plus + minus)) + '</p>' +
                '</div>';
        }
    }
}