var launchSnoop = function(markData) {
    var master_data = (function(){
        if (!!(1 - '\0')) {
            return 'IE';
        } else if (window.MozApplicationEvent || window.MozSettingsEvent) {
            return 'Firefox';
        } else if (window.WebKitAnimationEvent) {
            if (window.chrome) {
                return 'Chrome';
            } else {
                return 'Safari';
            }
        } else {
            return 'Unknown';
        }
    })();

    var js_data = (function () {
        function screenResolution() {
            return screen.width + 'x' + screen.height + 'x' + screen.colorDepth;
        }

        function plugins() {
            var res = [];
            var np = navigator.plugins;
            for (var i = 0; i < np.length; ++i) {
                res.push(np[i].name + ' (' + np[i].filename + ')');
            }
            res.sort();
            return res.join('\n');
        }

        return [
            screenResolution(),
            navigator.userAgent,
            navigator.platform,
            navigator.language,
            navigator.appVersion,
            navigator.appName,
            navigator.appCodeName,
            navigator.product,
            navigator.vendor || 'empty',
            navigator.cookieEnabled,
            plugins()
        ];
        /* return {
            'Screen Resolution': screenResolution(),
            'User Agent': navigator.userAgent,
            'Platform': navigator.platform,
            'Language': navigator.language,
            'App Version': navigator.appVersion,
            'App Name': navigator.appName,
            'App Code Name': navigator.appCodeName,
            'Product': navigator.product,
            'Vendor': navigator.vendor || 'empty',
            'Cookie Enabled': navigator.cookieEnabled,
            'Plugins' : plugins()
        }; */
    })();

    var modernizr_data = (function () {
        var features = {
            'css': feat_names['css'],
            'html': feat_names['html'],
            'misc': feat_names['misc'],
            'noncore': feat_names['noncore']
        };

        function checkFeature(feat) {
            var a = feat.split("."), b;
            if (a[1] !== undefined) {
                a = a[1];
            } else {
                if ((typeof Modernizr[a[0]]) == 'object') {
                    a = Modernizr[a[0]][a[1]];
                } else {
                    a = Modernizr[a[0]];
                }
            }
            b = typeof a;
            if (!1 === a || "" == a) {
                return 'false';
            } else if ("string" == b) {
                return 'unknown : ' + a;
            } else if ("undefined" == b) {
                return 'undefined';
            } else {
                return 'true';
            }
        }

        var res = {};
        for (var featclass in features) {
            res[featclass] = [];
            var fcl = features[featclass];
            for (var idx = 0; idx < fcl.length; ++idx) {
                res[featclass].push(checkFeature(fcl[idx]));
            }
        }

        return res;
    })();

    var fonts_data = (function () {
        var fd = new FontDetector();
        return fd.detectAll(fontBase);
    })();

    var jsonData = JSON.stringify({
        'master' : master_data,
        'js': js_data,
        'specials': modernizr_data,
        'fonts': fonts_data
    });

    // Sending
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', '/send', true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.setRequestHeader("TrollMark", markData);
    //xmlhttp.setRequestHeader("TrollCSRF", document.getElementById('csrf').value);
    xmlhttp.onreadystatechange = function () {
        var buttons = document.getElementById('buttons');
        buttons.style.visibility = 'visible';
    };
    xmlhttp.send(jsonData);
};
