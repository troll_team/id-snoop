// Gaining
var best_mark = undefined, all_marks = undefined;
var troll_mark = "TROLL_MARK";
trans_tab = {
    'userData' : 'UserData',
    'cookieData' : 'Cookies',
    'localData' : 'Local Storage',
    'globalData' : 'Global Storage',
    'sessionData' : 'Session Storage',
    'dbData' : 'DataBase Storage',
    'windowData' : 'Window',
    'historyData' : 'CSS History',
    'pngData' : 'PNG',
    'etagData' : 'Etag',
    'cacheData' : 'Cache',
    'lsoData' : 'Flash Cookies',
    'slData' : 'Silverlight'
};

// The Show
ec = new evercookie({});
ec.get(troll_mark, function(best_mark, all_marks) {
    document.getElementById('mleg').innerHTML = 'Marks (' + best_mark + ')';
    var ll = new List(document.getElementById('marks'));
    for (var item in all_marks) {
        if (all_marks[item] !== undefined && all_marks[item].substr(0, 3) == 'xzx') {
            if (item in trans_tab) {
                ll.addLine(trans_tab[item], all_marks[item]);
            } else {
                ll.addLine(item, all_marks[item]);
            }
        }
    }
});


